package controller;

import view.GUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import model.*;

public class Controller {
	private static GUI gui;
	private static Polinom p1;
	private static Polinom p2;
	
	private static Monom processMonom(String s){
		Monom m = new Monom();
		if(s.contains("x")){
			String[] temp = s.split("x");
			String coef;;
			String pow;
			if(temp.length <= 1){
				pow = "^1";
				if(temp.length == 0)
					coef = "1";
				else
					coef = temp[0];
			}	
			else{
				coef = temp[0];
				pow = temp[1];
			}
			if(coef.length() == 0)
				m.setCoef(1.0);
			else{
				if((coef.length() == 1) && (coef.charAt(0) == '-'))
					m.setCoef(-1.0);
				else{
					if((coef.length() == 1) && (coef.charAt(0) == '+'))
						m.setCoef(1.0);
					else{
						try {
							m.setCoef(Double.parseDouble(coef));
						} catch (NumberFormatException e) {
							return null;
						}
					}
				}
			}
			if(pow.length() == 0)
				m.setPow(1);
			else{
				if(pow.length() == 1)
					// nu poate fi puterea doar de 1 caracter (trebuie cel putin "^k")
					return null;
				else{
					if(pow.charAt(0) != '^')
						return null;
					else{
						try {
							m.setPow(Integer.parseInt(pow.substring(1)));
						} catch (NumberFormatException e) {
							return null;
						}
					}
				}
			}
		}
		else{
			try {
				m.setCoef(Double.parseDouble(s));
			} catch (NumberFormatException e) {
				return null;
			}
		}
		return m;
	}
	
	private static Polinom processString(String s) {
		Polinom p = new Polinom();
		
		String aux = "";
		for(int i = 0; i<s.length(); i++){
			char c = s.charAt(i);
			if((c == '-') || (c == '+')){
				if((i>0)&&(s.charAt(i-1) != '^'))
					aux += 'a';
			}
			if((Character.isDigit(c)) || (c == '-') || (c == '+') || (c == 'x') || (c == '^') || (c == '.')){
				aux += c;
			}
		}
		s = aux;
		
		String[] monomi = s.split("a");
		
		for(String i:monomi){
			if(i.length() > 0){
				Monom m = processMonom(i);
				if(m == null)
					return null;
				else
					p.addMonom(m);
			}
		}
		
		if(p.getMonoame().isEmpty())
			return null;
		return p;
	}
	
	@SuppressWarnings("deprecation")
	public static void main(String[] args) {
		p1 = new Polinom();
		p2 = new Polinom();
		gui = new GUI();
		gui.show();
		
		// -------------------------------- A D U N A R E -------------------------------
		gui.getButonAdunare().addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				p1 = processString(gui.readTextBox1());
				p2 = processString(gui.readTextBox2());
				gui.setLabel2("");
				if((p1 != null) && (p2 != null)){
					gui.setLabel(p1.adunare(p2).toString());
				}
				else
					gui.setLabel("Invalid input.");
				
			}
		});
		
		// -------------------------------- S C A D E R E -------------------------------
		gui.getButonScadere().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				p1 = processString(gui.readTextBox1());
				p2 = processString(gui.readTextBox2());
				gui.setLabel2("");
				if((p1 != null) && (p2 != null)){
					gui.setLabel(p1.scadere(p2).toString());
				}
				else
					gui.setLabel("Invalid input.");
				
			}
		});
		
		// -------------------------------- I N M U L T I R E -------------------------------
		gui.getButonInmultire().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				p1 = processString(gui.readTextBox1());
				p2 = processString(gui.readTextBox2());
				gui.setLabel2("");
				if((p1 != null) && (p2 != null)){
					gui.setLabel(p1.inmultire(p2).toString());
				}
				else
					gui.setLabel("Invalid input.");
				
			}
		});
		
		// -------------------------------- I M P A R T I R E -------------------------------
		gui.getButonImpartire().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				p1 = processString(gui.readTextBox1());
				p2 = processString(gui.readTextBox2());
				if((p1 != null) && (p2 != null)){
					Polinom imp[] = p1.impartire(p2);
					
					gui.setLabel(imp[0].toString());
					if(!imp[1].getMonoame().isEmpty())
						gui.setLabel2("Rest: " + imp[1].toString() + " / " + imp[2].toString());
					else
						gui.setLabel2("Rest: 0");
				}
				else
					gui.setLabel("Invalid input.");
			}
		});
		
		// -------------------------------- D E R I V A R E -------------------------------
		gui.getButonDerivare().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				p1 = processString(gui.readTextBox1());
				gui.setLabel2("");
				if(p1 != null){
					gui.setLabel(p1.derivare().toString());
				}
				else
					gui.setLabel("Invalid input.");
			}
		});
		
		// -------------------------------- I N T E G R A R E -------------------------------
		gui.getButonIntegrare().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				p1 = processString(gui.readTextBox1());
				if(p1 != null){
					gui.setLabel(p1.integrare().toString() + " + c");
					gui.setLabel2("");
				}
				else
					gui.setLabel("Invalid input.");
			}
		});
		
	}
	
}
