package view;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.*;

public class GUI extends JFrame{
	// requested setting an ID by JFrame
	private static final long serialVersionUID = 1L;
	
	private JButton butonAdunare;
	private JButton butonScadere;
	private JButton butonInmultire;
	private JButton butonImpartire;
	private JButton butonDerivare;
	private JButton butonIntegrare;
	
	private JTextField tb1, tb2;
	
	private JLabel l,l2;
	
	
	public GUI(){
		// getting the screen resolution in order to place the window in the middle
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		
		int width = 678, height = 195;
		setSize(width, height);
		setLocation(screenSize.width / 2 - width / 2, screenSize.height / 2 - 195);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLayout(null);
		setResizable(false);
		setTitle("Polinoame");
		
		// BUTTONS
		butonAdunare = new JButton("Adunare");
		butonAdunare.setLocation(10, 120);
		butonAdunare.setSize(100,30);
		
		butonScadere = new JButton("Scadere");
		butonScadere.setLocation(120, 120);
		butonScadere.setSize(100,30);
		
		butonInmultire = new JButton("Inmultire");
		butonInmultire.setLocation(230, 120);
		butonInmultire.setSize(100,30);
		
		butonImpartire = new JButton("Impartire");
		butonImpartire.setLocation(340, 120);
		butonImpartire.setSize(100,30);
		
		butonDerivare = new JButton("Derivare");
		butonDerivare.setLocation(450, 120);
		butonDerivare.setSize(100,30);
		
		butonIntegrare = new JButton("Integrare");
		butonIntegrare.setLocation(560, 120);
		butonIntegrare.setSize(100,30);
		
		add(butonAdunare);
		add(butonScadere);
		add(butonInmultire);
		add(butonImpartire);
		add(butonDerivare);
		add(butonIntegrare);
		
		
		// TEXTBOXES
		
		tb1 = new JTextField();
		tb1.setLocation(10, 10);
		tb1.setSize(650, 30);
		
		tb2 = new JTextField();
		tb2.setLocation(10, 44);
		tb2.setSize(650, 30);
		
		add(tb1);
		add(tb2);
		
		// LABELS
		
		l = new JLabel("<Result>");
		l.setLocation(14, 77);
		l.setSize(430, 30);
		add(l);
		
		l2 = new JLabel("");
		l2.setLocation(400, 77);
		l2.setSize(430, 30);
		add(l2);
		
	}
	
	public JButton getButonAdunare() {
		return butonAdunare;
	}
	
	public JButton getButonScadere() {
		return butonScadere;
	}

	public JButton getButonInmultire() {
		return butonInmultire;
	}


	public JButton getButonImpartire() {
		return butonImpartire;
	}


	public JButton getButonDerivare() {
		return butonDerivare;
	}


	public JButton getButonIntegrare() {
		return butonIntegrare;
	}
	
	public String readTextBox1() {
		return tb1.getText();
	}
	
	public String readTextBox2() {
		return tb2.getText();
	}
	
	public void setLabel(String s) {
		l.setText(s);
	}
	
	public void setLabel2(String s) {
		l2.setText(s);
	}
	
}
