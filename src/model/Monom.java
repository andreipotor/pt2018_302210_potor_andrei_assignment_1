package model;

import java.text.DecimalFormat;

public class Monom implements Comparable<Monom>
{
    private int pow;
    private Double coef;
    
    // pentru afisarea cu nr variabil de zecimale, de la 0 la 2
    DecimalFormat decimalFormat = new DecimalFormat("#.##");
    
    public Monom()
    {
    	coef = 0.0;
        pow = 0;
    }
    
    public Monom(Double coef, int pow)
    {
    	this.coef = coef;
        this.pow = pow;
    }
    
    public int getPow(){
        return pow;
    }
    
    public void setPow(int val){
        this.pow = val;
    }
    
    public Double getCoef(){
        return coef;
    }
    
    public void setCoef(Double val){
        this.coef = val;
    }
    
    public Monom addMonomeOfSamePow(Monom m) {
		return new Monom(coef + m.getCoef(), this.pow);
    }
    
    public Monom multiplyWithMonom(Monom m) {
		return new Monom(coef * m.getCoef(), this.pow + m.getPow());
    }
    
    public Monom divideByMonom(Monom m) {
		return new Monom(coef / m.getCoef(), this.pow - m.getPow());
    }
    
    public Monom derive() {
    	if(pow != 0)
    		return new Monom(coef * pow, pow - 1);
    	return new Monom(0.0,0);
    }
    
    public  Monom integrate() {
    	if(pow == -1)
    		return new Monom(0.0,0);
		return new Monom(coef / (pow + 1), pow + 1);
    }
    
    private String sign() {
		if(coef >= 0)
			return "+";
		else
			return "";	
	}
    
    public String toString(){
    	switch(pow) {
	    	case 0: return sign() + decimalFormat.format(coef);
	    	case 1: 
	    		if((coef != 1)&&(coef != -1))
	    			return sign() + decimalFormat.format(coef) + "x";
	    		else
	    			return (sign() + decimalFormat.format(coef)).substring(0, 1) + "x";
	    	default: 
	    		if((coef != 1)&&(coef != -1))
	    			return sign() + decimalFormat.format(coef) + "x^" + String.valueOf(pow);
	    		else
	    			return (sign() + decimalFormat.format(coef)).substring(0, 1) + "x^" + String.valueOf(pow);
    	}   		
    }

	@Override
	public int compareTo(Monom o) {
		return o.pow - this.pow;
	}
}
