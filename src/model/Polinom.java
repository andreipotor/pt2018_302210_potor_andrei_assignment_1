package model;
import java.util.*;

public class Polinom
{
    // instance variables - replace the example below with your own
    private List<Monom> monoame;

    public Polinom(){
    	monoame = new ArrayList<Monom>();
    }
    
    public Polinom(List<Monom> listaMonoame) {
    	monoame = listaMonoame;
    }
    
    public List<Monom> getMonoame(){
        return monoame;
    }
    
    // metoda care elimina monoamele de aceeasi putere si sorteaza monoamele in functie de putere
    private void processList() {
    	// eliminam elementele nule
    	removeZeros();
    	// adunam monoamele de aceeasi putere
    	mergeDuplicates();
    	// sortam monoamele in functie de putere - descrescator
    	Collections.sort(monoame);
    }
    
    private void removeZeros(){
    	for(int i = 0; i<monoame.size(); i++){
    		if(monoame.get(i).getCoef() == 0)
    			monoame.remove(i);
    	}
    }
    
    // metoda care aduna monoamele care au aceeasi putere
    private void mergeDuplicates() {
    	List<Monom> aux = new ArrayList<Monom>();
    	boolean found;
    	for(Monom i: monoame) {
    		found = false;
    		for(int j = 0; j<aux.size(); j++) {
    			if(i.getPow() == aux.get(j).getPow()) {
    				found = true;
    				Monom temp = i.addMonomeOfSamePow(aux.get(j));
    				if(temp.getCoef() != 0)
    					aux.set(j, temp);
    				else
    					aux.remove(j);
    			}
    		}
    		if(!found)
				aux.add(i);
    	}
    	
    	monoame = aux;
    }
    
    public void addMonom(Monom val){
    	monoame.add(val);
    	processList();
    }
    
    public String toString(){
    	String s = "";
    	for(Monom m:monoame)
    		s += m.toString();
    	if(s == "")
    		s = "<Polinom Gol>";
    	return s;
    }
    
// ------------------------------------------------- O P E R A T I I ----------------------------------------------
    
    // ADUNARE
    public Polinom adunare(Polinom p) {
    	List<Monom> aux = new ArrayList<Monom>();
    	for(Monom j: monoame)
    		aux.add(new Monom(j.getCoef(), j.getPow()));
		for(Monom j: p.getMonoame())
			aux.add(new Monom(j.getCoef(), j.getPow()));
    	Polinom res = new Polinom(aux);
    	res.processList();
    	return res;
    }
	
    // SCADERE
    public Polinom scadere(Polinom p) {
    	List<Monom> aux = new ArrayList<Monom>();
    	for(Monom j: monoame)
    		aux.add(new Monom(j.getCoef(), j.getPow()));
		for(Monom j: p.getMonoame())
			aux.add(new Monom(-j.getCoef(), j.getPow()));
    	Polinom res = new Polinom(aux);
    	res.processList();
    	return res;
    }
    
    // INMULTIRE
    public Polinom inmultire(Polinom p) {
    	List<Monom> aux = new ArrayList<Monom>();
    	for(Monom i: monoame)
    		for(Monom j: p.getMonoame())
    			aux.add(i.multiplyWithMonom(j));
    	Polinom res = new Polinom(aux);
    	res.processList();
    	return res;
    }
    
    // IMPARTIRE
    public Polinom[] impartire(Polinom p){
    	Polinom p1 = new Polinom(monoame);
    	Polinom p2 = new Polinom(p.getMonoame());
    	Polinom sus = new Polinom();
    	Polinom susCuCareInmultim = new Polinom();
    	Polinom jos = new Polinom();
    	
    	while((!p1.getMonoame().isEmpty())&&(p2.getMonoame().get(0).getPow() <= p1.getMonoame().get(0).getPow())){
    		sus.addMonom((p1.getMonoame().get(0).divideByMonom(p2.getMonoame().get(0))));
    		susCuCareInmultim = new Polinom();
    		susCuCareInmultim.addMonom((p1.getMonoame().get(0).divideByMonom(p2.getMonoame().get(0))));
    		jos = p2.inmultire(susCuCareInmultim);
    		p1 = p1.scadere(jos);
    	}
    	
    	Polinom[] res = new Polinom[3];
    	res[0] = sus;
    	res[1] = p1;
    	res[2] = p2;
    	
    	return res;
    }
    
    // DERIVARE
    public Polinom derivare() {
    	List<Monom> aux = new ArrayList<Monom>();
    	for(Monom i: monoame)
    		aux.add(i.derive());
    	Polinom p = new Polinom(aux);
    	p.processList();
    	return p;
    }
    
    // INTEGRARE
    public Polinom integrare() {
    	List<Monom> aux = new ArrayList<Monom>();
    	for(Monom i: monoame)
    		aux.add(i.integrate());
    	Polinom p = new Polinom(aux);
    	p.processList();
    	return p;
    }
}














